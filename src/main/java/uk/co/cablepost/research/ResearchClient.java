package uk.co.cablepost.research;


import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry;
import uk.co.cablepost.research.research_machine.ResearchMachineScreen;

@Environment(EnvType.CLIENT)
public class ResearchClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        ScreenRegistry.register(Research.RESEARCH_MACHINE_SCREEN_HANDLER, ResearchMachineScreen::new);
    }
}