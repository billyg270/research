package uk.co.cablepost.research.research_machine;

import com.mojang.blaze3d.systems.RenderSystem;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.Recipe;
import net.minecraft.screen.slot.Slot;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.registry.Registry;
import uk.co.cablepost.research.Research;
import uk.co.cablepost.research.reaserch_item.ResearchType;
import uk.co.cablepost.research.research_recipe.MultipleIngredient;
import uk.co.cablepost.research.research_recipe.ResearchRecipe;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class ResearchMachineScreen extends HandledScreen<ResearchMachineScreenHandler> {
    //A path to the gui texture. In this example we use the texture from the dispenser
    private static final Identifier TEXTURE = new Identifier(Research.MOD_ID, "textures/gui/container/research_machine.png");

    private DefaultedList<List<MultipleIngredient>> recipeInputs = DefaultedList.ofSize(16, new ArrayList<>());

    private int inputVarientCycle = 0;

    public ResearchMachineScreen(ResearchMachineScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
        this.backgroundWidth = 176;
        this.backgroundHeight = 256;
        this.titleY = 5;
        this.playerInventoryTitleY = 164;
    }

    @Override
    protected void drawBackground(MatrixStack matrices, float delta, int mouseX, int mouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShaderTexture(0, TEXTURE);
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        drawTexture(matrices, x, y, 0, 0, backgroundWidth, backgroundHeight);

        int selectedResearchType = handler.getSelectedResearchType();

        for(int m = 0; m < 9; m++) {
            if(recipeInputs.get(m).size() > 0) {
                drawResearchTypeSelectButton(
                        matrices,
                        m,
                        selectedResearchType == m,
                        x,
                        y,
                        7 + m * 18,
                        16,
                        mouseX,
                        mouseY
                );
            }
        }
        for(int m = 9; m < 16; m++) {
            if(recipeInputs.get(m).size() > 0) {
                drawResearchTypeSelectButton(
                        matrices,
                        m,
                        selectedResearchType == m,
                        x,
                        y,
                        7 + (m - 8) * 18,
                        16 + 18,
                        mouseX,
                        mouseY
                );
            }
        }

        int researchTime = handler.getResearchTime();
        drawTexture(matrices, x + 83, y + 113, 176, 113, 10, (int)Math.ceil(((float)researchTime / (float)ResearchMachineBlockEntity.RESEARCH_MAX_TIME) * 24f));

        inputVarientCycle += (int)Math.floor(delta * 1000);
        if(inputVarientCycle > 999999999){
            inputVarientCycle = 0;
        }

        textRenderer.draw(matrices, new TranslatableText("researchMachine.requiredInputs.text"), (float)x - 90, (float)y + 60 -17, 0xc6c6c6);
        for(int i = 0; i < recipeInputs.get(selectedResearchType).size(); i++){
            ItemStack[] matchingStacks = recipeInputs.get(selectedResearchType).get(i).ingredient.getMatchingStacks();
            int matchingStackIndex = (int)Math.floor(inputVarientCycle / 20000f) % matchingStacks.length;
            //textRenderer.draw(matrices, new TranslatableText(matchingStacks[matchingStackIndex].getTranslationKey()), (float)x - 110, (float)y + 60 + i * 17, 0xc6c6c6);
            textRenderer.draw(matrices, "x" + recipeInputs.get(selectedResearchType).get(i).count, (float)x - 70, (float)y + 60 + i * 17, 0x6060ff);

            MinecraftClient.getInstance().getItemRenderer().renderInGuiWithOverrides(matchingStacks[matchingStackIndex], x - 90, y + 55 + i * 17, 0, 0);
            RenderSystem.setShader(GameRenderer::getPositionTexShader);
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            RenderSystem.setShaderTexture(0, TEXTURE);
        }
    }

    void drawResearchTypeSelectButton(MatrixStack matrices, int researchTypeIndex, boolean selected, int bgX, int bgY, int x, int y, int mouseX, int mouseY){
        if(selected){
            if(mouseOverResearchTypeSelectButton(bgX, bgY, x, y, mouseX, mouseY)){
                drawTexture(matrices, bgX + x, bgY + y, 176, 18 * 3, 18, 18);
            }
            else{
                drawTexture(matrices, bgX + x, bgY + y, 176, 18 * 2, 18, 18);
            }
        }
        else{
            if(mouseOverResearchTypeSelectButton(bgX, bgY, x, y, mouseX, mouseY)){
                drawTexture(matrices, bgX + x, bgY + y, 176, 18 * 1, 18, 18);
            }
            else{
                drawTexture(matrices, bgX + x, bgY + y, 176, 18 * 0, 18, 18);
            }
        }

        MinecraftClient.getInstance().getItemRenderer().renderInGuiWithOverrides(new ItemStack(Research.RESEARCH_TYPES[researchTypeIndex].item), bgX + x + 1, bgY + y + 1, 0, 0);
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShaderTexture(0, TEXTURE);
    }

    boolean mouseOverResearchTypeSelectButton(int bgX, int bgY, int x, int y, int mouseX, int mouseY){
        return
                mouseX >= bgX + x &&
                mouseX <= bgX + x + 18 &&
                mouseY >= bgY + y &&
                mouseY <= bgY + y + 18
        ;
    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        if(client == null || client.player == null || client.player.isSpectator()){
            return super.mouseClicked(mouseX, mouseY, button);
        }

        int x = (this.width - this.backgroundWidth) / 2;
        int y = (this.height - this.backgroundHeight) / 2;

        for(int m = 0; m < 9; m++) {
            if(recipeInputs.get(m).size() > 0) {
                if(mouseOverResearchTypeSelectButton(x, y, 7 + m * 18, 16, (int)mouseX, (int)mouseY)){
                    client.player.playSound(SoundEvents.UI_BUTTON_CLICK, 0.3f, 1.0f);

                    PacketByteBuf buf = PacketByteBufs.create();
                    buf.writeVarInt(m);
                    ClientPlayNetworking.send(Research.UPDATE_SELECTED_RESEARCH_TYPE_PACKET_ID, buf);
                }
            }
        }
        for(int m = 9; m < 16; m++) {
            if(recipeInputs.get(m).size() > 0) {
                if(mouseOverResearchTypeSelectButton(x, y, 7 + (m - 8) * 18, 16 + 18, (int)mouseX, (int)mouseY)){
                    client.player.playSound(SoundEvents.UI_BUTTON_CLICK, 0.3f, 1.0f);

                    PacketByteBuf buf = PacketByteBufs.create();
                    buf.writeVarInt(m);
                    ClientPlayNetworking.send(Research.UPDATE_SELECTED_RESEARCH_TYPE_PACKET_ID, buf);
                }
            }
        }

        return super.mouseClicked(mouseX, mouseY, button);
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        renderBackground(matrices);
        super.render(matrices, mouseX, mouseY, delta);
        drawMouseoverTooltip(matrices, mouseX, mouseY);
    }

    @Override
    protected void drawMouseoverTooltip(MatrixStack matrices, int mouseX, int mouseY) {
        int x = (this.width - this.backgroundWidth) / 2;
        int y = (this.height - this.backgroundHeight) / 2;

        int selectedResearchType = handler.getSelectedResearchType();

        for(int i = 0; i < recipeInputs.get(selectedResearchType).size(); i++){
            ItemStack[] matchingStacks = recipeInputs.get(selectedResearchType).get(i).ingredient.getMatchingStacks();
            int matchingStackIndex = (int)Math.floor(inputVarientCycle / 20000f) % matchingStacks.length;

            if(mouseX >= x - 90 && mouseX <= x - 70){
                if(mouseY >= y + 55 + i * 17 && mouseY <= y + 55 + i * 17 + 16){
                    this.renderTooltip(matrices, matchingStacks[matchingStackIndex], mouseX, mouseY);
                }
            }
        }

        super.drawMouseoverTooltip(matrices, mouseX, mouseY);
    }

    @Override
    protected void init() {
        super.init();
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
        updateRecipeInputs();
    }

    private void updateRecipeInputs(){
        recipeInputs = DefaultedList.ofSize(16, new ArrayList<>());

        assert client != null;
        assert client.world != null;

        for(ResearchType researchType : Research.RESEARCH_TYPES) {
            //List<ResearchRecipe> recipes = client.world.getRecipeManager().listAllOfType(Research.RESEARCH_RECIPE_TYPE);
            Optional<? extends Recipe> recipeOptional = client.world.getRecipeManager().get(new Identifier(Research.MOD_ID, researchType.id));

            if (recipeOptional.isEmpty()) {
                continue;
            }

            Recipe recipeFound = recipeOptional.get();

            if(recipeFound instanceof ResearchRecipe researchRecipe) {
                List<MultipleIngredient> thisRecipeInputs = new ArrayList<>();

                for (Ingredient ingredient : researchRecipe.input) {
                    //ItemStack[] matchingStacks = ingredient.getMatchingStacks();

                    boolean needToAdd = true;
                    for (MultipleIngredient existing : thisRecipeInputs) {
                        if (Objects.equals(existing.ingredient.toJson().toString(), ingredient.toJson().toString())) {
                            existing.count++;
                            needToAdd = false;
                        }
                    }

                    if (needToAdd) {
                        thisRecipeInputs.add(new MultipleIngredient(ingredient));
                    }
                }

                recipeInputs.set(researchType.index, thisRecipeInputs);
            }
        }
    }
}