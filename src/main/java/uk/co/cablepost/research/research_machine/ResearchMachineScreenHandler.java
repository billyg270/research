package uk.co.cablepost.research.research_machine;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ArrayPropertyDelegate;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.util.Identifier;
import uk.co.cablepost.research.Research;

import java.util.Objects;

public class ResearchMachineScreenHandler extends ScreenHandler {
    private final Inventory inventory;
    PropertyDelegate propertyDelegate;

    public ResearchMachineScreenHandler(int syncId, PlayerInventory playerInventory) {
        this(
                syncId,
                playerInventory,
                new SimpleInventory(ResearchMachineBlockEntity.SLOT_COUNT),
                new ArrayPropertyDelegate(ResearchMachineBlockEntity.PROPERTY_DELEGATE_SIZE)
        );
    }

    public ResearchMachineScreenHandler(int syncId, PlayerInventory playerInventory, Inventory inventory, PropertyDelegate propertyDelegate) {
        super(Research.RESEARCH_MACHINE_SCREEN_HANDLER, syncId);
        checkSize(inventory, ResearchMachineBlockEntity.SLOT_COUNT);
        this.inventory = inventory;
        this.propertyDelegate = propertyDelegate;
        inventory.onOpen(playerInventory.player);
        this.addProperties(propertyDelegate);

        //Input
        for (int m = 0; m < 3; m++) {
            for (int l = 0; l < 9; l++) {
                this.addSlot(new Slot(inventory, l + m * 9, 8 + l * 18, 58 + m * 18));
            }
        }

        //Output
        for (int m = 0; m < 9; m++) {
            this.addSlot(new Slot(inventory, (3 * 9) + m, 8 + m * 18, 140));
        }

        //The player inventory
        for (int m = 0; m < 3; m++) {
            for (int l = 0; l < 9; ++l) {
                this.addSlot(new Slot(playerInventory, l + m * 9 + 9, 8 + l * 18, 174 + m * 18));
            }
        }
        //The player Hotbar
        for (int m = 0; m < 9; m++) {
            this.addSlot(new Slot(playerInventory, m, 8 + m * 18, 232));
        }

    }

    @Override
    public boolean canUse(PlayerEntity player) {
        return this.inventory.canPlayerUse(player);
    }

    @Override
    public ItemStack transferSlot(PlayerEntity player, int invSlot) {
        ItemStack newStack = ItemStack.EMPTY;
        Slot slot = this.slots.get(invSlot);
        if (slot.hasStack()) {
            ItemStack originalStack = slot.getStack();
            newStack = originalStack.copy();
            if (invSlot < this.inventory.size()) {
                if (!this.insertItem(originalStack, this.inventory.size(), this.slots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.insertItem(originalStack, 0, this.inventory.size(), false)) {
                return ItemStack.EMPTY;
            }

            if (originalStack.isEmpty()) {
                slot.setStack(ItemStack.EMPTY);
            } else {
                slot.markDirty();
            }
        }

        return newStack;
    }

    public int getSelectedResearchType(){
        return propertyDelegate.get(ResearchMachineBlockEntity.PROPERTY_DELEGATE_SELECTED_RESEARCH_TYPE_INDEX);
    }

    public void setSelectedResearchType(int type){
        propertyDelegate.set(ResearchMachineBlockEntity.PROPERTY_DELEGATE_SELECTED_RESEARCH_TYPE_INDEX, type);
    }

    public int getResearchTime(){
        return propertyDelegate.get(ResearchMachineBlockEntity.PROPERTY_DELEGATE_RESEARCH_TIME_INDEX);
    }
}