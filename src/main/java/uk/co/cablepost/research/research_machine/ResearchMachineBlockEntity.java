package uk.co.cablepost.research.research_machine;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.Packet;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.recipe.*;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.research.Research;
import uk.co.cablepost.research.research_recipe.ResearchRecipe;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class ResearchMachineBlockEntity extends BlockEntity implements ResearchMachineInventory, SidedInventory, NamedScreenHandlerFactory {

    public static int RESEARCH_MAX_TIME = 20 * 60;


    public static int SLOT_COUNT = (3 * 9) + 9;
    private final DefaultedList<ItemStack> items = DefaultedList.ofSize(SLOT_COUNT, ItemStack.EMPTY);
    public int selectedResearchType;
    public int researchTime;

    public ResearchRecipe recipe = null;
    public Boolean canCraft = null;

    public static ArrayList<ItemStack>[] REQUIRED_INPUT = new ArrayList[16];

    public ResearchMachineBlockEntity(BlockPos pos, BlockState state) {
        super(Research.RESEARCH_MACHINE_BLOCK_ENTITY, pos, state);
        researchTime = 0;
    }

    @Override
    public void readNbt(NbtCompound tag) {
        super.readNbt(tag);

        Inventories.readNbt(tag, items);
        selectedResearchType = tag.getInt("selectedResearchType");
        researchTime = tag.getInt("researchTime");
    }

    @Override
    public void writeNbt(NbtCompound tag) {
        Inventories.writeNbt(tag, items);
        tag.putInt("selectedResearchType", selectedResearchType);
        tag.putInt("researchTime", researchTime);

        super.writeNbt(tag);
    }

    @Nullable
    @Override
    public Packet<ClientPlayPacketListener> toUpdatePacket() {
        return BlockEntityUpdateS2CPacket.create(this);
    }

    @Override
    public NbtCompound toInitialChunkDataNbt() {
        return createNbt();
    }

    public static int PROPERTY_DELEGATE_SIZE = 2;
    public static int PROPERTY_DELEGATE_SELECTED_RESEARCH_TYPE_INDEX = 0;
    public static int PROPERTY_DELEGATE_RESEARCH_TIME_INDEX = 1;

    private final PropertyDelegate propertyDelegate = new PropertyDelegate() {
        @Override
        public int get(int index) {
            if(index == PROPERTY_DELEGATE_SELECTED_RESEARCH_TYPE_INDEX){
                return selectedResearchType;
            }
            if(index == PROPERTY_DELEGATE_RESEARCH_TIME_INDEX){
                return researchTime;
            }
            return 0;
        }

        @Override
        public void set(int index, int value) {
            if(index == PROPERTY_DELEGATE_SELECTED_RESEARCH_TYPE_INDEX){
                selectedResearchType = value;
                markDirty();
            }
        }

        //this is supposed to return the amount of integers you have in your delegate, in our example only one
        @Override
        public int size() {
            return PROPERTY_DELEGATE_SIZE;
        }
    };

    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory playerInventory, PlayerEntity player) {
        return new ResearchMachineScreenHandler(syncId, playerInventory, this, propertyDelegate);
    }

    @Override
    public Text getDisplayName() {
        return new TranslatableText(getCachedState().getBlock().getTranslationKey());
    }

    @Override
    public DefaultedList<ItemStack> getItems() {
        return items;
    }

    public static void clientTick(World world, BlockPos pos, BlockState state, ResearchMachineBlockEntity blockEntity) {
    }

    public static void serverTick(World world, BlockPos pos, BlockState state, ResearchMachineBlockEntity blockEntity) {
        if(blockEntity.recipe == null){
            blockEntity.updateRecipe();
        }

        if(blockEntity.canCraft == null){
            blockEntity.updateCanCraft();
        }

        if(blockEntity.canCraft){
            blockEntity.researchTime ++;
            if(blockEntity.researchTime >= ResearchMachineBlockEntity.RESEARCH_MAX_TIME){
                blockEntity.researchTime = 0;
                blockEntity.craft();
            }
        }else{
            blockEntity.researchTime = 0;
        }
    }

    public void craft(){
        ItemStack craftingResult = recipe.craft(this);
        DefaultedList<ItemStack> remainder = recipe.getRemainder(this);

        List<ItemStack> input = new ArrayList<>();

        for(int j = 0; j < 3 * 9; ++j) {
            input.add(getStack(j));
        }

        if(!spaceInOutput(DefaultedList.ofSize(1, craftingResult))){
            researchTime = ResearchMachineBlockEntity.RESEARCH_MAX_TIME - 20;//try again in a sec
            return;
        }

        getLeftoverInputs(input);

        addItemToOutput(craftingResult);

        markDirty();
    }

    public boolean getLeftoverInputs(List<ItemStack> inputs){
        List<Ingredient> ingredients = recipe.getIngredients();
        for(Ingredient ingredient : ingredients){
            if(!isIngredientSatisfied(ingredient, inputs)){
                return false;
            }
        }

        return true;
    }

    public boolean isIngredientSatisfied(Ingredient ingredient, List<ItemStack> inputs){
        for (int i = 0; i < inputs.size(); i++) {
            if (ingredient.test(inputs.get(i))) {
                inputs.get(i).decrement(1);
                if(inputs.get(i).getCount() == 0){
                    inputs.set(i, ItemStack.EMPTY);
                }

                return true;
            }
        }

        return false;
    }

    public boolean spaceInOutput(DefaultedList<ItemStack> toAdd2){
        //copy toAdd stacks to temp location to test on
        DefaultedList<ItemStack> toAdd = DefaultedList.ofSize(toAdd2.size(), ItemStack.EMPTY);

        for(int i = 0; i < toAdd.size(); i++){
            toAdd.set(i, toAdd2.get(i).copy());
        }

        //copy output stacks to temp location to test on
        DefaultedList<ItemStack> output = DefaultedList.ofSize(9, ItemStack.EMPTY);

        for(int i = 3 * 9; i < 3 * 9 + 9; i++){
            output.set(i - 3 * 9, getStack(i).copy());
        }

        //try inserting all stacks

        for (ItemStack toAddItemStack : toAdd) {
            if (toAddItemStack.isEmpty()) {
                continue;
            }

            for (int j = 0; j < output.size(); j++) {
                ItemStack outputItemStack = output.get(j);

                if (!outputItemStack.isEmpty() && !ItemStack.canCombine(toAddItemStack, outputItemStack)) {
                    continue;
                }

                int spaceLeftInOutputSlot = toAddItemStack.getMaxCount();
                if (!outputItemStack.isEmpty()) {
                    spaceLeftInOutputSlot = outputItemStack.getMaxCount() - outputItemStack.getCount();
                }

                int canAddHere = Math.min(toAddItemStack.getCount(), spaceLeftInOutputSlot);

                if (outputItemStack.isEmpty()) {
                    output.set(j, toAddItemStack.copy());
                } else {
                    outputItemStack.increment(canAddHere);
                }

                toAddItemStack.decrement(canAddHere);

                if (toAddItemStack.isEmpty()) {
                    break;
                }
            }

            if (!toAddItemStack.isEmpty()) {
                return false;
            }
        }

        return true;
    }

    private void addItemToOutput(ItemStack itemStack){
        for(int i = 3 * 9; i < 3 * 9 + 9; i++){
            ItemStack outputItemStack = getStack(i);

            if(!outputItemStack.isEmpty() && !ItemStack.canCombine(itemStack, outputItemStack)){
                continue;
            }

            int spaceLeftInOutputSlot = itemStack.getMaxCount();
            if(!outputItemStack.isEmpty()){
                spaceLeftInOutputSlot = outputItemStack.getMaxCount() - outputItemStack.getCount();
            }

            int canAddHere = Math.min(itemStack.getCount(), spaceLeftInOutputSlot);

            if(outputItemStack.isEmpty()){
                setStack(i, itemStack.copy());
            }
            else{
                outputItemStack.increment(canAddHere);
            }

            itemStack.decrement(canAddHere);

            if(itemStack.isEmpty()){
                break;
            }
        }
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        int[] result = new int[getItems().size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = i;
        }

        return result;
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, Direction direction) {
        //return slot < 3 * 9;
        if(slot >= 3 * 9){
            return false;
        }

        if(recipe == null){
            return false;
        }

        List<ItemStack> inputs = new ArrayList<>();

        for(int j = 0; j < 3 * 9; ++j) {
            inputs.add(getStack(j).copy());
        }

        List<Ingredient> ingredients = recipe.getIngredients();
        for(Ingredient ingredient : ingredients){
            if(!isIngredientSatisfied(ingredient, inputs)){
                if(ingredient.test(stack)){
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction direction) {
        return slot >= 3 * 9;
    }

    @Override
    public void markDirty() {
        boolean serverSide = this.hasWorld() && !this.getWorld().isClient();

        if(serverSide){
            updateRecipe();
            updateCanCraft();
        }

        super.markDirty();

        if (serverSide) {
            ((ServerWorld) world).getChunkManager().markForUpdate(getPos());
        }
    }

    public void updateRecipe(){
        recipe = null;

        assert world != null;
        Optional<? extends Recipe> optional = Objects.requireNonNull(world.getServer()).getRecipeManager().get(new Identifier(Research.MOD_ID, Research.RESEARCH_TYPES[selectedResearchType].id));

        if (optional.isEmpty()) {
            return;
        }

        Recipe recipeFound = optional.get();

        if(recipeFound instanceof ResearchRecipe researchRecipe) {
            recipe = researchRecipe;
        }
    }

    public void updateCanCraft(){
        if(recipe == null){
            canCraft = false;
            return;
        }

        /*
        RecipeMatcher recipeMatcher = new RecipeMatcher();
        int i = 0;

        for(int j = 0; j < 3 * 9; ++j) {
            ItemStack itemStack = getStack(j);
            if (!itemStack.isEmpty()) {
                ++i;
                recipeMatcher.addInput(itemStack, 1);
            }
        }

        canCraft = i == recipe.input.size() && recipeMatcher.match(recipe, null);
        */

        List<ItemStack> input = new ArrayList<>();

        for(int j = 0; j < 3 * 9; ++j) {
            input.add(getStack(j).copy());
        }

        canCraft = getLeftoverInputs(input);
    }
}
