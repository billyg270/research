package uk.co.cablepost.research.research_recipe;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.RecipeSerializer;
import net.minecraft.recipe.ShapedRecipe;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.collection.DefaultedList;

import java.util.Iterator;

public class ResearchRecipeSerializer implements RecipeSerializer<ResearchRecipe> {

    private ResearchRecipeSerializer() {
    }

    public static final ResearchRecipeSerializer INSTANCE = new ResearchRecipeSerializer();

    public static final String ID = "research_recipe";


    @Override
    // Turns json into Recipe
    public ResearchRecipe read(Identifier id, JsonObject json) {
        DefaultedList<Ingredient> ingredients = getIngredients(JsonHelper.getArray(json, "ingredients"));
        ItemStack result = ShapedRecipe.outputFromJson(JsonHelper.getObject(json, "result"));

        return new ResearchRecipe(id, result, ingredients);
    }

    private static DefaultedList<Ingredient> getIngredients(JsonArray json) {
        DefaultedList<Ingredient> defaultedList = DefaultedList.of();

        for(int i = 0; i < json.size(); ++i) {
            JsonElement je = json.get(i);
            Ingredient ingredient = Ingredient.fromJson(je);
            if (!ingredient.isEmpty()) {
                defaultedList.add(ingredient);
            }
        }

        return defaultedList;
    }

    @Override
    // Turns Recipe into PacketByteBuf
    public void write(PacketByteBuf packetData, ResearchRecipe recipe) {
        packetData.writeVarInt(recipe.input.size());

        for (Ingredient ingredient : recipe.input) {
            ingredient.write(packetData);
        }

        packetData.writeItemStack(recipe.output);
    }

    @Override
    // Turns PacketByteBuf into Recipe
    public ResearchRecipe read(Identifier id, PacketByteBuf packetData) {
        int inputSize = packetData.readVarInt();
        DefaultedList<Ingredient> ingredients = DefaultedList.ofSize(inputSize, Ingredient.EMPTY);

        ingredients.replaceAll(ignored -> Ingredient.fromPacket(packetData));

        ItemStack result = packetData.readItemStack();
        return new ResearchRecipe(id, result, ingredients);
    }
}