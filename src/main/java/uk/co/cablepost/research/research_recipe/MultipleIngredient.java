package uk.co.cablepost.research.research_recipe;

import net.minecraft.recipe.Ingredient;

public class MultipleIngredient {
    public Ingredient ingredient;
    public int count;

    public MultipleIngredient(Ingredient i){
        this.ingredient = i;
        this.count = 1;
    }

    public MultipleIngredient(Ingredient i, int c){
        this.ingredient = i;
        this.count = c;
    }
}
