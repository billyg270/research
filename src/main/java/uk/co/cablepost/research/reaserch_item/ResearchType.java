package uk.co.cablepost.research.reaserch_item;

import java.util.Locale;

public class ResearchType {
    public int index;
    public String colour;
    public String id;
    public ResearchItem item;

    public ResearchType(int index, String colour, ResearchItem item){
        colour = colour.toLowerCase(Locale.ROOT);

        this.index = index;
        this.colour = colour;
        this.id = colour + "_research";
        this.item = item;
    }
}
