package uk.co.cablepost.research;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemGroup;
import net.minecraft.recipe.RecipeType;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import uk.co.cablepost.research.reaserch_item.ResearchItem;
import uk.co.cablepost.research.reaserch_item.ResearchType;
import uk.co.cablepost.research.research_machine.ResearchMachineBlock;
import uk.co.cablepost.research.research_machine.ResearchMachineBlockEntity;
import uk.co.cablepost.research.research_machine.ResearchMachineScreen;
import uk.co.cablepost.research.research_machine.ResearchMachineScreenHandler;
import uk.co.cablepost.research.research_recipe.ResearchRecipe;
import uk.co.cablepost.research.research_recipe.ResearchRecipeSerializer;

public class Research implements ModInitializer {

    public static final String MOD_ID = "research";

    public static final Identifier RESEARCH_MACHINE_IDENTIFIER = new Identifier(MOD_ID, "research_machine");
    public static final Block RESEARCH_MACHINE_BLOCK = new ResearchMachineBlock(FabricBlockSettings.of(Material.METAL).strength(5.0f));
    public static BlockEntityType<ResearchMachineBlockEntity> RESEARCH_MACHINE_BLOCK_ENTITY;

    public static final ScreenHandlerType<ResearchMachineScreenHandler> RESEARCH_MACHINE_SCREEN_HANDLER = ScreenHandlerRegistry.registerSimple(RESEARCH_MACHINE_IDENTIFIER, ResearchMachineScreenHandler::new);

    public static RecipeType<ResearchRecipe> RESEARCH_RECIPE_TYPE;

    public static ResearchType[] RESEARCH_TYPES = new ResearchType[] {
            new ResearchType(0, "white", new ResearchItem(new FabricItemSettings().group(ItemGroup.MISC))),
            new ResearchType(1, "orange", new ResearchItem(new FabricItemSettings().group(ItemGroup.MISC))),
            new ResearchType(2, "magenta", new ResearchItem(new FabricItemSettings().group(ItemGroup.MISC))),
            new ResearchType(3, "light_blue", new ResearchItem(new FabricItemSettings().group(ItemGroup.MISC))),
            new ResearchType(4, "yellow", new ResearchItem(new FabricItemSettings().group(ItemGroup.MISC))),
            new ResearchType(5, "lime", new ResearchItem(new FabricItemSettings().group(ItemGroup.MISC))),
            new ResearchType(6, "pink", new ResearchItem(new FabricItemSettings().group(ItemGroup.MISC))),
            new ResearchType(7, "gray", new ResearchItem(new FabricItemSettings().group(ItemGroup.MISC))),
            new ResearchType(8, "light_gray", new ResearchItem(new FabricItemSettings().group(ItemGroup.MISC))),
            new ResearchType(9, "cyan", new ResearchItem(new FabricItemSettings().group(ItemGroup.MISC))),
            new ResearchType(10, "purple", new ResearchItem(new FabricItemSettings().group(ItemGroup.MISC))),
            new ResearchType(11, "blue", new ResearchItem(new FabricItemSettings().group(ItemGroup.MISC))),
            new ResearchType(12, "brown", new ResearchItem(new FabricItemSettings().group(ItemGroup.MISC))),
            new ResearchType(13, "green", new ResearchItem(new FabricItemSettings().group(ItemGroup.MISC))),
            new ResearchType(14, "red", new ResearchItem(new FabricItemSettings().group(ItemGroup.MISC))),
            new ResearchType(15, "black", new ResearchItem(new FabricItemSettings().group(ItemGroup.MISC)))
    };

    public static final Identifier UPDATE_SELECTED_RESEARCH_TYPE_PACKET_ID = new Identifier(MOD_ID, "update_selected_research_type");

    @Override
    public void onInitialize() {

        //=== Research items ===

        for(ResearchType researchType : RESEARCH_TYPES) {
            Registry.register(Registry.ITEM, new Identifier(MOD_ID, researchType.id), researchType.item);
        }

        //=== Research machine ===

        RESEARCH_MACHINE_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                RESEARCH_MACHINE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(ResearchMachineBlockEntity::new, RESEARCH_MACHINE_BLOCK).build(null)
        );

        Registry.register(Registry.BLOCK, RESEARCH_MACHINE_IDENTIFIER, RESEARCH_MACHINE_BLOCK);
        Registry.register(Registry.ITEM, RESEARCH_MACHINE_IDENTIFIER, new BlockItem(RESEARCH_MACHINE_BLOCK, new FabricItemSettings().group(ItemGroup.DECORATIONS)));

        //=== Recipe type ===

        Registry.register(
                Registry.RECIPE_SERIALIZER,
                new Identifier(MOD_ID, ResearchRecipeSerializer.ID),
                ResearchRecipeSerializer.INSTANCE
        );

        RESEARCH_RECIPE_TYPE = Registry.register(
                Registry.RECIPE_TYPE,
                new Identifier(MOD_ID, ResearchRecipe.Type.ID),
                ResearchRecipe.Type.INSTANCE
        );

        //=== Select Research Type Packet ===

        ServerPlayNetworking.registerGlobalReceiver(UPDATE_SELECTED_RESEARCH_TYPE_PACKET_ID, (server, player, handler, buf, responseSender) -> {
            int researchType = buf.readVarInt();

            server.executeSync(() -> {
                if (!player.isSpectator() && player.currentScreenHandler instanceof ResearchMachineScreenHandler screenHandler) {
                    screenHandler.setSelectedResearchType(researchType);
                }
            });
        });
    }
}